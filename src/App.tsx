import React from 'react';
import { Router, RouteComponentProps } from '@reach/router';

import './App.css';
import { Login } from './Pages/Auth/Login';
import { Home } from './Pages/Home/Home';
import { NotFound } from './Pages/NotFound/NotFound';

const RouterPage = (props: { pageComponent: JSX.Element } & RouteComponentProps) => props.pageComponent;

const App: React.FC = () => {
  return (
    <Router>
      <RouterPage path="login" pageComponent={<Login />} />
      <RouterPage path="/" pageComponent={<Home />} />
      <RouterPage default pageComponent={<NotFound />} />
    </Router>
  );
};

export default App;
